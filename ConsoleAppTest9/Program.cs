﻿using System;

namespace ConsoleAppTest9
{
    class Program
    {
        //this is a test
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int n = 10;
            int[] test = new int[n];
            int i = 0;
            while ( i < n )
            {
                test[i] = ++i;
            }
            Random x = new Random();
            int j;
            for (i = 0; i < n; i++)
            {
                j = x.Next(n);
                if (i != j)
                {
                    test[i] ^= test[j];
                    test[j] ^= test[i];
                    test[i] ^= test[j];

                }
            }
            for (i = 0; i < n; i++)
            {
                Console.Write($"{test[i]},");

            }
            Console.ReadKey();
        }
    }
}
